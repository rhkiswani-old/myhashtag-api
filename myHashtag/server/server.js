var loopback = require('loopback');
var boot = require('loopback-boot');
var express     =    require("express");
var multer      =    require('multer');
var done        =    false;
var app = module.exports = loopback();
var path = require('path');
var fs = require('fs');
var server = require('./server');
var hashtag = app.models.hashtag;
// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname);

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');

    console.log('Web server listening at: %s', app.get('url'));
  });
};


app.use(loopback.static("/profilePicture",path.resolve(__dirname, '../uploads')));

app.use(multer({ dest: './uploads/',
 rename: function (fieldname, filename) {
    return filename+Date.now();
  },
  onFileUploadStart: function (file) {
    console.log(file.originalname + ' is starting ...')
  },
  onFileUploadComplete: function (file) {
    console.log(file.fieldname + ' uploaded to  ' + file.path)
    done=true;
  }
}));

app.post('/profilePicture',function(req,res){
  if(done==true){
    console.log(req.files);
    res.end("File uploaded.");
    return "uplodaed";
  }
});

// start the server if `$ node server.js`
if (require.main === module) {
  app.start();
}

app.get('/getProfilePicture', function (request, res) {  
  console.log("/getProfilePicture ===>");
  console.log(request.query.id);
  var hashtag = app.models.Hashtag
  hashtag.findOne({where: {hashtagId: request.query.id}}, 
    function (err, hashtag) {
      res.writeHead(200, {'Content-Type': 'image/jpg' });
      if(hashtag != null){
        res.end(hashtag.profilePicture, 'binary');
        return ;
      }
      res.end();
    }
  );
  

});


app.get('/Notifer', function (request, res) {  
  res.send('Sent');
  var http = require('http');
  
  var data = {
    "collapseKey":"applice",
    "delayWhileIdle":true,
    "timeToLive":3,
    "data":{
      "message":request.query.msg,"title":request.query.title
      },
    "registration_ids":[request.query.gcmId]
  };

  var dataString =  JSON.stringify(data);
  var headers = {
    'Authorization' : 'key=AIzaSyCNvfBpORw7lMKejgJQ9LCbeSOE1RcfvaU',
    'Content-Type' : 'application/json',
    'Content-Length' : dataString.length
  };

  var options = {
    host: 'android.googleapis.com',
    port: 80,
    path: '/gcm/send',
    method: 'POST',
    headers: headers
  };

  //Setup the request 
  var req = http.request(options, function(res) {
    res.setEncoding('utf-8');

    var responseString = '';

    res.on('data', function(data) {
      responseString += data;
    });

    res.on('end', function() {
    	try{
  	    var resultObject = JSON.parse(responseString);
  	    print(responseString);
  	    console.log(resultObject);
    	}catch (e) {
  		console.log(responseString);
    	}

    });
    console.log('STATUS: ' + res.statusCode);
    console.log('HEADERS: ' + JSON.stringify(res.headers));

  });

  req.on('error', function(e) {
    // TODO: handle error.
    console.log('error : ' + e.message + e.code);
  });

  req.write(dataString);
  req.end();
});
